<%@page import="com.cdm.controller.module.WebModule"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Put Responsable</title>
</head>
<body>
	<form action="putResponsableConfirmed" method="post">
	<input value="Valider" type="submit" />
	<table>
		<tr>
			<td>Module Id</td>
			<td>Public</td>
			<td>Libelle</td>
			<td>Enseignant</td>
		</tr>
		<%
			List<WebModule> lModules = (List<WebModule>) request.getAttribute("Modules");
			List<String> lResponsables = (List<String>) request.getAttribute("Responsables");
			for(WebModule lModule : lModules){
		%>
				<tr>
					<td><%=lModule.getModuleId() %></td>
					<td><%=lModule.getPublic() %></td>
					<td><%=lModule.getLibelle() %></td>
					<td>
						<select name="<%=lModule.getModuleId() %>">
							<%
								//chargement des responsables sur la liste deroulante pour pouvoir en choisir un responsable pour un module
								for(String lResponsable : lResponsables){
							%>
									<option value="<%=lResponsable %>" <%if(lResponsable.equals(lModule.getResponsable())) out.print("selected='selected'"); %>><%=lResponsable %></option>
							<%
								}
							%>
						</select>
					</td>
				</tr>
		<%
			}
		%>
	</table>
	<input value="Valider" type="submit" />
	</form>
</body>
</html>