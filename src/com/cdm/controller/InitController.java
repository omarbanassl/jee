package com.cdm.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class InitController implements Controller {
	
	private static String mPage = "/jsp/initialisation.jsp";
	
	@Override
	public String action(HttpServletRequest pRequest, HttpServletResponse pResponse) throws ServletException, IOException {
		
		
		return mPage;
	}
}
