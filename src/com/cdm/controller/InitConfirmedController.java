package com.cdm.controller;

import java.sql.PreparedStatement;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Course;
import Model.Module;
import Model.ModuleDAO;
import Model.ModuleDAOImpl;

import com.cdm.servlets.MyServlet;

public class InitConfirmedController implements Controller {
	
	private static String mPage = "/jsp/initialisation.jsp";
	
	@Override
	public String action(HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception {
		
		StringBuilder lResults = new StringBuilder();
		
		ModuleDAO moduleDAO = ModuleDAOImpl.getInstance();	
		
		ArrayList<Module> moduleList = (ArrayList<Module>) moduleDAO.getModules();
		
		for(Module lModuleItem : moduleList){// on parcours la liste des modules pour mettre � jour la base de donn�es
			
			Course lCourse = moduleDAO.getCourse(lModuleItem.getId());
			
			String lModule = lCourse.getId();
			String lLibelle = lCourse.getCourseName().getCourseName().getText();
			String lPublic = lCourse.getId();//on reccupere l'id du cours pour pouvoir extrere le public vis�
			lPublic=lPublic.substring(0, 6);
			lPublic=lPublic.replace("CO_","");
			lPublic=lPublic.replace("TL","LSI");
			lPublic=lPublic.replace("TZ","SH");
			lPublic=lPublic.replace("TE","EII");
			lPublic=lPublic.replace("TO","OPT");
			lPublic=lPublic.replace("TI","IMR");
			lPublic=lPublic.replace("3","1");
			lPublic=lPublic.replace("4","2");
			lPublic=lPublic.replace("5","3");
			if(lModule.length() > 20){
				lModule = lModule.substring(0, 20);
			}
			if(lPublic.length() > 20){
				lPublic = lPublic.substring(0, 20);
			}
			if(lLibelle.length() > 50){
				lLibelle = lLibelle.substring(0, 50);
			}
			lResults.append(lModule + " / " + lLibelle + " : Charg� \n");
			// TODO GET PUBLIC table MODULE
			PreparedStatement lPrepareStatement = null;
			try {
				//inserer les valeur recuperer depuis les fichiers xml dans la base de donn�e
				String lQuery = "INSERT INTO MODULE (module, libelle, public) VALUES (\"" + lModule + "\", \"" + lLibelle + "\", \"" +lPublic + "\")";
				lPrepareStatement = MyServlet.sConnection.prepareStatement(lQuery);
				lPrepareStatement.executeUpdate();
			}catch(Exception ex){
				System.out.println(ex.getMessage());
			}
			finally {
				if(lPrepareStatement != null){
					lPrepareStatement.close();
				}
			}
		}
		if(!"".equals(lResults)){
			lResults.append("Chargement Termin� avec success !!! \n");
			pRequest.setAttribute("Results", lResults.toString());
		}
		
		return mPage;
	}
}
