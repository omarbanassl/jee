package com.cdm.controller;

import java.sql.PreparedStatement;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cdm.servlets.MyServlet;

public class PutResponsableConfirmedController extends PutResponsableController {
	
	@Override
	public String action(HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception {
		
		Map<?, ?> lParameters = pRequest.getParameterMap();
		
		for(Entry<?, ?> lEntry : lParameters.entrySet()){
			String lKey = (String) lEntry.getKey();
			String lValue = ((String[]) lEntry.getValue())[0];
			System.out.println(lKey + " / " + lValue);
			if(!"VIDE".equals(lValue)){
				PreparedStatement lPrepareStatement = null;
				try {
					String lQuery = "update module set responsable=\"" + lValue + "\" where module=\"" + lKey + "\"";
					lPrepareStatement = MyServlet.sConnection.prepareStatement(lQuery);
					lPrepareStatement.executeUpdate();
				}catch(Exception ex){
					System.out.println(ex.getMessage());
				}
				finally {
					if(lPrepareStatement != null){
						lPrepareStatement.close();
					}
				}
			}
			
		}
		
		return super.action(pRequest, pResponse);
	}
}
