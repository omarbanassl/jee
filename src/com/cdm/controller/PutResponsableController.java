package com.cdm.controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cdm.controller.module.WebModule;
import com.cdm.servlets.MyServlet;

public class PutResponsableController implements Controller {
	
	private static String mPage = "/jsp/putresponsable.jsp";
	
	@Override
	public String action(HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception {
		
		PreparedStatement lPrepareStatement = null;
		ResultSet lResultSet = null;
		try {
			String lQuery = "select * from module"; 
			lPrepareStatement = MyServlet.sConnection.prepareStatement(lQuery);
			lResultSet = lPrepareStatement.executeQuery();
			
			List<WebModule> lModules = new ArrayList<WebModule>();
			
			while(lResultSet.next()){
				String lModule = lResultSet.getString("module");
				String lPublic = lResultSet.getString("public");
				String lLibelle = lResultSet.getString("libelle");
				String lResponsable = lResultSet.getString("responsable");
				
				lModules.add(new WebModule(lModule, lPublic, lLibelle, lResponsable));
			}
			
			pRequest.setAttribute("Modules", lModules);
			
		}catch(Exception ex){
			System.out.println(ex.getMessage());
		}
		finally {
			if(lResultSet != null){
				lResultSet.close();
			}
			if(lPrepareStatement != null){
				lPrepareStatement.close();
			}
		}
		
		try {
			String lQuery = "select login from enseignant";
			lPrepareStatement = MyServlet.sConnection.prepareStatement(lQuery);
			lResultSet = lPrepareStatement.executeQuery();
			
			List<String> lResponsables = new ArrayList<String>();
			lResponsables.add("VIDE");
			while(lResultSet.next()){
				String lNom = lResultSet.getString("login");
				
				lResponsables.add(lNom);
			}
			
			pRequest.setAttribute("Responsables", lResponsables);
			
		}catch(Exception ex){
			System.out.println(ex.getMessage());
		}
		finally {
			if(lResultSet != null){
				lResultSet.close();
			}
			if(lPrepareStatement != null){
				lPrepareStatement.close();
			}
		}
		
		
		return mPage;
	}
}
