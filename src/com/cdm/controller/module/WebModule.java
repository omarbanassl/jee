package com.cdm.controller.module;

public class WebModule {
	private String mModuleId;
	private String mPublic;
	private String mLibelle;
	private String mResponsable;
	public WebModule(String pModuleId, String pPublic, String pLibelle,
			String pResponsable) {
		super();
		mModuleId = pModuleId;
		mPublic = pPublic;
		mLibelle = pLibelle;
		mResponsable = pResponsable;
	}
	public String getModuleId() {
		return mModuleId;
	}
	public void setModuleId(String pModuleId) {
		mModuleId = pModuleId;
	}
	public String getPublic() {
		return mPublic;
	}
	public void setPublic(String pPublic) {
		mPublic = pPublic;
	}
	public String getLibelle() {
		return mLibelle;
	}
	public void setLibelle(String pLibelle) {
		mLibelle = pLibelle;
	}
	public String getResponsable() {
		return mResponsable;
	}
	public void setResponsable(String pResponsable) {
		mResponsable = pResponsable;
	}
}
