package com.cdm.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cdm.controller.Controller;
import com.cdm.controller.InitConfirmedController;
import com.cdm.controller.InitController;
import com.cdm.controller.PutResponsableConfirmedController;
import com.cdm.controller.PutResponsableController;

/**
 * Servlet implementation class MyServlet
 */
public class MyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public static Connection sConnection;
	
	private Map<String, Controller> mRequestToPageJsp = new HashMap<String, Controller>();
	
	
       
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		System.out.println("init");
		
		try {
			//connection � la base de donn�es
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			sConnection = DriverManager.getConnection("jdbc:mysql://localhost/PLANNINGS?user=root&password=omarbezzanou");
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		InitController lInitController = new InitController();
		InitConfirmedController lInitConfirmedController = new InitConfirmedController();
		PutResponsableController lPutResponsableController = new PutResponsableController();
		PutResponsableConfirmedController lPutResponsableConfirmedController = new PutResponsableConfirmedController();
		mRequestToPageJsp.put("/JEE/cdm/initialisation", lInitController);
		mRequestToPageJsp.put("/JEE/cdm/initialisationConfimed", lInitConfirmedController);
		mRequestToPageJsp.put("/JEE/cdm/putResponsable", lPutResponsableController);
		mRequestToPageJsp.put("/JEE/cdm/putResponsableConfirmed", lPutResponsableConfirmedController);
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String lRequestURI = request.getRequestURI();
		System.out.println("doGet : " + lRequestURI);
		Controller lController = mRequestToPageJsp.get(lRequestURI);
		if(lController != null){
			try {
				request.getRequestDispatcher(lController.action(request, response)).forward(request, response);
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("doPost");
		doGet(request, response);
	}

}
